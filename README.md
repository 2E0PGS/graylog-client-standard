# Graylog client

A .NET Standard 2.0 client for Graylog server HTTP GELF input.

This library supports basic auth and HTTPS. Ideal for use with a nginx reverse proxy.

The library is designed to log and forget. If the logging server is down it wont crash or slow your program.