﻿/*
    Copyright (C) <2019-2022>  <Peter Stevenson (2E0PGS)>

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

using Graylog.Client.Enums;
using Graylog.Client.Models;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Net.Http;
using System.Net.Http.Headers;

namespace Graylog.Client
{
    public class GraylogClient
    {
        public string Address { get; set; }
        public string Username { get; set; }
        public string Password { get; set; }
        public string Facility { get; set; }
        public bool ClientSideTimestamps { get; set; }

        public GraylogClient(string address, string facility = null, bool clientSideTimestamps = true, string username = null, string password = null)
        {
            Address = address;
            Username = username;
            Password = password;
            Facility = facility;
            ClientSideTimestamps = clientSideTimestamps;
        }

        public void Log(string shortMessage, string fullMessage = "", SeveritylevelEnum? severitylevel = null)
        {
            var requestModel = new RequestModel();
            if (severitylevel != null)
            {
                requestModel.Level = (int)severitylevel.Value;
            }
            requestModel.ShortMessage = shortMessage;
            requestModel.FullMessage = fullMessage;

            SendToGraylogMerge(requestModel);
        }

        public void LogExtra(string shortMessage, string fullMessage = "", SeveritylevelEnum? severitylevel = null, object extra = null)
        {
            JObject jObject = JObject.Parse(JsonConvert.SerializeObject(extra));

            var newJObject = new JObject();

            foreach (JProperty prop in jObject.Properties())
            {
                if (!newJObject.ContainsKey("_" + prop.Name))
                {
                    newJObject.Add("_" + prop.Name, prop.Value);
                }
            }

            dynamic dynRequest = JsonConvert.DeserializeObject<dynamic>(JsonConvert.SerializeObject(newJObject));

            var requestModel = new RequestModel();
            if (severitylevel != null)
            {
                requestModel.Level = (int)severitylevel.Value;
            }
            requestModel.ShortMessage = shortMessage;
            requestModel.FullMessage = fullMessage;

            SendToGraylogMerge(requestModel, dynRequest);
        }

        public void Log(string shortMessage, Exception exception)
        {
            var requestModel = new RequestModel();
            requestModel.Level = (int)SeveritylevelEnum.Error;
            requestModel.IsException = 1;
            requestModel.ShortMessage = shortMessage;
            requestModel.FullMessage = exception.ToString();

            SendToGraylogMerge(requestModel);
        }

        public void Log(Exception exception)
        {
            var requestModel = new RequestModel();
            requestModel.Level = (int)SeveritylevelEnum.Error;
            requestModel.IsException = 1;
            requestModel.ShortMessage = exception.Message.ToString();
            requestModel.FullMessage = exception.ToString();

            SendToGraylogMerge(requestModel);
        }

        private void SendToGraylogMerge(RequestModel requestModel, object extra = null)
        {
            var httpClient = new HttpClient();
            requestModel.Facility = Facility;
            requestModel.Version = "1.1";
            requestModel.Host = System.Environment.MachineName;

            // If a basic auth user and pass is supplied add it into the request headers. Useful for reverse proxy for auth.
            if (!string.IsNullOrEmpty(Username) && !string.IsNullOrEmpty(Password))
            {
                httpClient.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Basic", Convert.ToBase64String(
                    System.Text.Encoding.ASCII.GetBytes(
                        string.Format("{0}:{1}", Username, Password))));
            }

            // If they want client timestamps then inject it.
            if (ClientSideTimestamps == true)
            {
                requestModel.Timestamp = DateTime.Now.ToUniversalTime().Subtract(new DateTime(1970, 1, 1, 0, 0, 0, DateTimeKind.Utc)).TotalSeconds;
            }

            JObject jObject = JObject.Parse(JsonConvert.SerializeObject(requestModel));

            if (extra != null)
            {
                JObject jObject2 = JObject.Parse(JsonConvert.SerializeObject(extra));

                jObject.Merge(jObject2, new JsonMergeSettings
                {
                    MergeArrayHandling = MergeArrayHandling.Union
                });
            }

            httpClient.PostAsJsonAsync(Address, jObject);
        }
    }
}
